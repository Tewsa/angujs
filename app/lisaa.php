<?php
// Luetaan HTTP-post-kutsu muuttujaan.
$postdata = file_get_contents('php://input');
// Erotellaan muuttujasta lähetetty data.
$request = json_decode($postdata);

// POST-kutsun "kentät" on luettavissa kätevästi nyt request-objektista.
$sukunimi = $request->sukunimi;
$etunimi = $request->etunimi;
$email = $request->email;

try {
    // Avataan tietokantayhteys.
    $tietokanta = new PDO('mysql:host=localhost;dbname=henkilorekisteri;charset=utf8','root','');
    $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // Muodostetaan parametroitu sql-kysely, joka lisää uuden henkiön tiedot tietokantaan.
    $kysely = $tietokanta->prepare("INSERT INTO henkilo(etunimi,sukunimi,email) VALUES (:etunimi,:sukunimi,:email)");
    // Liitetään sql-lauseessa annetut parametrit lomakkeella syötettyihin arvoihin.
    $kysely->bindValue(':etunimi',$etunimi,PDO::PARAM_STR);
    $kysely->bindValue(':sukunimi',$sukunimi,PDO::PARAM_STR);
    $kysely->bindValue(':email',$email,PDO::PARAM_STR);
    $kysely->execute();
    // Palautetaan lisätyn tietueen id.
    print $tietokanta->lastInsertId();
} catch (PDOException $pdoex) {
    // Tietokannan käsittelyssä tapahtui virhe. Palautetaan tilakoodi 500 (internal server error) sekä
    // virheen kuvaus.
    http_response_code(500);
    print $pdoex->getMessage();
}