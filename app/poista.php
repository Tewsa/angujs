<?php
// Luetaan HTTP-post-kutsu muuttujaan.
$postdata = file_get_contents('php://input');
// Erotellaan muuttujasta lähetetty data.
$request = json_decode($postdata);

// POST-kutsun "kentät" on luettavissa kätevästi nyt request-objektista.
$id = $request->id;

try {
    // Avataan tietokantayhteys.
    $tietokanta = new PDO('mysql:host=localhost;dbname=henkilorekisteri;charset=utf8','root','');
    $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // Muodostetaan parametroitu sql-kysely, joka lisää uuden henkiön tiedot tietokantaan.
    $kysely = $tietokanta->prepare("DELETE FROM henkilo WHERE id=:id");
    // Liitetään sql-lauseessa annetut parametrit lomakkeella syötettyihin arvoihin.
    $kysely->bindValue(':id',$id,PDO::PARAM_INT);
    $kysely->execute();
    // Palautetaan poistetun tietueen id.
    print $id;
} catch (PDOException $pdoex) {
    // Tietokannan käsittelyssä tapahtui virhe. Palautetaan tilakoodi 500 (internal server error) sekä
    // virheen kuvaus.
    http_response_code(500);
    print $pdoex->getMessage();
}