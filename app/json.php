<?php
try {
    //Avataan tietokantayhteys.
    $tietokanta = new PDO('mysql:host=localhost;dbname=henkilorekisteri;charset=utf8','root','');
    //Muodostetaan sql-kysely.
    $sql='SELECT * FROM henkilo ORDER BY id';
    //Suoritetaan kysely.
    $kysely = $tietokanta->query($sql);
    //Muodostetaan tietokannan taulusta haetusta tiedosta json-viesti.
    $json = json_encode(array('henkilot' => $kysely->fetchAll(PDO::FETCH_ASSOC)),JSON_PRETTY_PRINT);
    print $json; 
} catch (PDOException $pdoex) {
    http_response_code(500);
}