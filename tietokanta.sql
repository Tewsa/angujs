/**/
drop database henkilorekisteri;
create database henkilorekisteri;

use henkilorekisteri;

create table henkilo(
    id int primary key auto_increment,
    etunimi varchar(50) not null,
    sukunimi varchar(50) not null,
    email varchar(255) not null 
);

insert into henkilo (etunimi, sukunimi, email) values ('Jouni','Juntunen','jjuntune@oamk.fi');
insert into henkilo (etunimi, sukunimi, email) values ('Pekka','Ojala','pojala@oamk.fi');
insert into henkilo (etunimi, sukunimi, email) values ('Liisa','Auer','lauer@oamk.fi');



/**
 * Author:  n5rate00
 * Created: Dec 1, 2016
 */

