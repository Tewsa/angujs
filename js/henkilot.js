var app = angular.module('myApp', []);

app.controller('henkiloCtrl', function ($scope, $http) {
    $scope.naytaLomake = false;
    $http.get("app/json.php").then(function (response) {
        $scope.myData = response.data.henkilot;
    });

    $scope.lisaa = function () {
        $scope.naytaLomake = !$scope.naytaLomake;
    };

    $scope.peruuta = function () {
        $scope.naytaLomake = false;
        document.getElementById("lomake").reset();
    };

    $scope.tallenna = function () {
        //console.log(window.location.href);
        //var url jscript poisto index.php.
        //ajax  
        var url = window.location.href;
        url = url.split(window.location.href)[0];
        var request = $http({
            method: "post",
            url: url + 'app/lisaa.php',
            data: {
                sukunimi: $scope.sukunimi,
                etunimi: $scope.etunimi,
                email: $scope.email},
            headers: {'Content-type': 'application/x-www-form-urlencoded'}
        }).then(function (response) {
            console.log("Post onnistui!");
            $scope.myData.push({
                "id": response.data,
                "sukunimi": $scope.sukunimi,
                "etunimi": $scope.etunimi,
                "email": $scope.email
            });
            $scope.sukunimi = "";
            $scope.etunimi = "";
            $scope.email = "";
        }, function (response) {
            console.log("Post ei onnistunut!");
            alert("Virhe " + response.data);

        });
        $scope.naytaLomake = false;
    };

    $scope.poista = function (id) {
        var url = window.location.href;
        url = url.split(window.location.href)[0];
        var request = $http({
            method: "post",
            url: url + 'app/poista.php',
            data: {
                id: id },
            headers: {'Content-type': 'application/x-www-form-urlencoded'}
        }).then(function (response) {
            console.log("Post delete onnistui!");
            for (var i=0;i<$scope.myData.length;i++) {
                if($scope.myData[i].id===response.data) {
                    $scope.myData.splice(i,1);
                    break;
                }
            }
        }, function (response) {
            console.log("Post delete ei onnistunut!");
            alert("Virhe " + response.data);

        });
        $scope.naytaLomake = false;
    };
});
