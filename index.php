<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>AngularJS DB</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.6/angular.js"></script>
        <script src="js/henkilot.js"></script>
        <link href="css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <h2>Henkilörekisterijuttuhomma</h2>
        <div ng-app="myApp" ng-controller="henkiloCtrl">
            <button ng-click="lisaa()">Lisää</button>
            <div ng-show="naytaLomake">
                <form id="lomake">
                    <div>
                        <label>Sukunimi: </label>
                        <input ng-model="sukunimi">
                    </div>
                    <div>
                        <label>Etunimi: </label>
                        <input ng-model="etunimi">
                    </div>
                    <div>
                        <label>Email: </label>
                        <input ng-model="email">
                    </div>
                    <div>
                        <button ng-click="tallenna()">Tallenna</button>
                        <button ng-click="peruuta()">Peruuta</button>
                    </div>
                </form>
            </div>
            <table>
                <tr>
                    <td>ID</td>
                    <td>Sukunimi</td>
                    <td>Etunimi</td>
                    <td>Email</td>
                </tr>
                <tr ng-repeat="x in myData">
                    <td>{{x.id}}</td>
                    <td>{{x.sukunimi}}</td>
                    <td>{{x.etunimi}}</td>
                    <td>{{x.email}}</td>
                    <td><a ng-click="poista(x.id)">x</a></td>
                </tr>
            </table>
        </div>
    </body>
</html>
